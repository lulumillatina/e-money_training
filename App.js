import * as React from 'react';
import { View, Text, Button, StyleSheet} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { createDrawerNavigator } from '@react-navigation/drawer';
import MainTabScreen from './screen/MainTabScreen';

// function SignInScreen({navigation}) {
//     const [Email, setEmail] = React.useState('');
//     const [Password, setPassword] = React.useState('');
//     const { SignIn } = React.useContext(AuthContext);
//   return (
//     <View style={{ flex: 1, justifyContent: 'center' , borderWidth:1}}>
//       <Text style={styles.SignInText}>e-money</Text>
//       <TextInput 
//       placeholder="Email" 
//       style={styles.InputBox}
//       value={Email}
//       onChangeText={setEmail}/>
//       <TextInput 
//       placeholder="Password" 
//       style={styles.InputBox}
//       value={Password}
//       onChangeText={setPassword}
//       />
//       <TouchableOpacity style={styles.Button} onPress={() => SignIn({ Email, Password })}>
//         <Text style={styles.TextButton}>Sign In</Text>
//       </TouchableOpacity>
//       <TouchableOpacity onPress={() => navigation.navigate('Registration')}>
//         <Text style={styles.TextButton}>Registration</Text>
//       </TouchableOpacity>
//     </View>
//   );
// }

// function RegistrationScreen({navigation}) {
//   return (
//     <View style={{ flex: 1, justifyContent: 'center' }}>
//       <TextInput style={styles.InputBox} placeholder="Email"/>
//       <TextInput style={styles.InputBox} placeholder="Password"/>
//       <TextInput style={styles.InputBox} placeholder="Name"/>
//       <TextInput style={styles.InputBox} placeholder="Phone Number"/>
//       <TouchableOpacity style={styles.Button} onPress={() => navigation.navigate('SigIn')}><Text style={styles.TextButton}>SUBMIT</Text></TouchableOpacity>
//     </View>
//   );
// }

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const App = () => {
  return (  
    <NavigationContainer>
      <Stack.Navigator>
         <Stack.Screen name="MainTab" component={MainTabScreen}/>
      </Stack.Navigator>
    </NavigationContainer>

  );
}

export default App;

const styles = StyleSheet.create({
  SignInText: {
    fontSize : 30,
    color: "#4982C1",
    marginBottom: 10,
    textAlign: 'center'
  },
  InputBox: {
    backgroundColor: "white",
    marginBottom : 20,
    marginHorizontal :50,
    borderWidth : 1,
    borderColor : "#C3C3C3",
    
  },
  Button: {
    marginBottom : 20,
    marginHorizontal :50,
    backgroundColor: "#4982C1",
    padding :10
  },
  TextButton: {
    textAlign: 'center'
  }
});
