import * as React from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';

import HomeStackScreen from './HomeStackScreen';
import TransactionHistory from './TransactionHistory';
import ProfileScreen from './ProfileScreen';

const Tab = createMaterialBottomTabNavigator();

const MainTabScreen = () => (
<Tab.Navigator
initialRouteName="Home"
activeColor="#e91e63"
style={{ backgroundColor: 'tomato' }}
>
<Tab.Screen
  name="Home"
  component={HomeStackScreen}
  options={{
    tabBarLabel: 'Home',
    tabBarColor: '#009387',
    tabBarIcon: ({ color }) => (
      <Icon name="ios-home" color={color} size={26} />
    ),
  }}
/>
<Tab.Screen
  name="TransactionHistory"
  component={TransactionHistory}
  options={{
    tabBarLabel: 'TransactionHistory',
    tabBarIcon: ({ color }) => (
      <Icon name="ios-time" color={color} size={26} />
    ),
  }}
/>
<Tab.Screen
  name="Profile"
  component={ProfileScreen}
  options={{
    tabBarLabel: 'Profile',
    tabBarIcon: ({ color }) => (
      <Icon name="ios-person" color={color} size={26} />
    ),
  }}
/>
</Tab.Navigator>
)

export default MainTabScreen; 