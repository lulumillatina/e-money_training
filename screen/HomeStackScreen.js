import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import HomeScreen from './HomeScreen';
import TopUpScreen from './TopUpScreen';
import ScanQRScreen from './ScanQRScreen';
import TransferScreen from './TransferScreen';

const HomeStack = createStackNavigator();

const HomeStackScreen = () => (
<HomeStack.Navigator>
    <HomeStack.Screen name="Home" component={HomeScreen} />
    <HomeStack.Screen name="TopUp" component={TopUpScreen} />
    <HomeStack.Screen name="ScanQR" component={ScanQRScreen} />
    <HomeStack.Screen name="Transfer" component={TransferScreen} />
</HomeStack.Navigator>
)

export default HomeStackScreen; 